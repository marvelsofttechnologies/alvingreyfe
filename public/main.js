let navHeight = 100;


window.addEventListener("scroll", function () {
	scrollpos = window.scrollY;
	
	if (window.scrollY >= navHeight) {
		let menu = document.querySelector(".menu");
		menu.classList.add("scrolled");
		return;
	}
	if (window.scrollY < navHeight) {
		let menu = document.querySelector(".menu");
		menu.classList.remove("scrolled");
		return;
	}

	console.log(scrollpos);
});
