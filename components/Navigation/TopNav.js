import React from 'react'

export default function TopNav() {
    return (
        <div className="topnav">
        <ul className="topnavmenu">
          <li className="topnavlink"><a href="/">Home</a></li>
          <li className="topnavlink"><a href="register">Join AG</a></li>
          <li className="topnavlink"><a href="learn">Learn </a></li>
          <li className="topnavlink"><a href="technology">Technology</a></li>
          <li className="topnavlink"><a href="contact">Find Us</a></li>
          <li className="topnavlink"><a href="login">Sign In</a></li>
          {/* <li class="topnavlink"><a href="">Join KW</a></li> */}
        </ul>
      </div>
    )
}
