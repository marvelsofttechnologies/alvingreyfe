import Link from 'next/link'
import React from 'react'


export default function Footer() {
    return (
        <div>
            <div className="second-footer">
    <ul className="sec-title">
      <li className="top active">
        <a>AG</a><i className="fa fa-angle-down" />
        <ul className="dropdowns">
          <li><a href>About Us</a></li>
          <li><a href>Press</a></li>
          <li><a href>Career</a></li>
          <li><a href>Contact Us</a></li>
        </ul>
      </li>
    </ul>
    <ul className="sec-title">
      <li className="top">
        <a>Properties</a><i className="fa fa-angle-down" />
        <ul className="dropdowns">
          <li><a href>Commercial</a></li>
          <li><a href>Residential</a></li>
          <li><a href>Land</a></li>
          <li><a href>Homes</a></li>
        </ul>
      </li>
    </ul>
    <ul className="sec-title">
      <li className="top">
        <a>Realtors</a><i className="fa fa-angle-down" />
        <ul className="dropdowns">
          <li><a href>Join AG</a></li>
          <li><a href>Technology</a></li>
          <li><a href>Testimonial</a></li>
        </ul>
      </li>
    </ul>
    <ul className="sec-title">
      <li className="top">
        <a>Quick find</a><i className="fa fa-angle-down" />
        <ul className="dropdowns">
            <li><Link href='/terms.pdf'>Terms and Conditions</Link></li>
          <li><a href>Relators</a></li>
          <li><a href>Properties</a></li>
          <li><a href>Locations</a></li>
        </ul>
      </li>
    </ul>
    <ul className="sec-title">
      <li className="top">
        <a>Social</a><i className="fa fa-angle-down" />
        <ul className="dropdowns">
          <li>
            <a href="https://instagram.com/alvingreyandassociates?utm_medium=copy_link">Instagram</a>
          </li>
          <li>
            <a href="https://www.facebook.com/Alvin-Grey-and-Associates-118386450010938/">Facebook</a>
          </li>
          <li><a href>Twitter</a></li>
          <li><a href>LinkedIn</a></li>
        </ul>
      </li>
    </ul>
  </div>
  <div className="footer-logo">
    <img src="/Full-Logo-White-Background.png" alt />
  </div>
  <div className="third-footer">
  <p className="copyright-text">© 2021 Alvin Grey Inc.</p>
  </div>
        </div>
    )
}
