import Link from "next/link";
import React from "react";

export default function RegisterationPropmpt() {
  return (
    <div className="d-flex flex-column text-center">
        <i class="fas fa-check-double text-success h1"></i>
      <h5>
        Your registeration was successfull, and your email has been verified. 
        Proceed to Login
      </h5>
      <Link href='/login'>
      <button className="action-btn red-bgg mx-auto w-100 mt-2">Sign In</button>
      </Link>
    </div>
  );
}
