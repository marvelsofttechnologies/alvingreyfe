import Head from "next/head";
import Image from "next/image";
import Footer from "../components/Navigation/Footer";
import TopNav from "../components/Navigation/TopNav";
import Link from "next/link";
import { useState } from "react";

export default function Home() {
  const [tab, setTab] = useState("mission");
  return (
    <div>
      <div>
        <TopNav />
        <div className="menu">
          <div className="logo">
            <Link href="/">
              <svg
                width="198px"
                height="35px"
                viewBox="0 0 198 35"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
              >
                <title>Full Logo-White Background@1x</title>
                <g
                  id="Page-1"
                  stroke="none"
                  strokeWidth={1}
                  fill="none"
                  fillRule="evenodd"
                >
                  <g id="A4" transform="translate(-273.000000, -398.000000)">
                    <g
                      id="Full-Logo-White-Background"
                      transform="translate(273.000000, 398.000000)"
                    >
                      <rect
                        id="Rectangle"
                        fill="#000000"
                        x={0}
                        y={0}
                        width={8}
                        height={35}
                      />
                      <rect
                        id="Rectangle"
                        fill="#000000"
                        x={12}
                        y={0}
                        width={8}
                        height={35}
                      />
                      <rect
                        id="Rectangle"
                        fill="#000000"
                        x={24}
                        y={0}
                        width={8}
                        height={35}
                      />
                      <rect
                        id="Rectangle"
                        fill="#CD1A1D"
                        x={36}
                        y={0}
                        width={162}
                        height={35}
                      />
                      <path
                        d="M49.464,26.448 L50.712,23.112 L57.288,23.112 L58.608,26.448 L63.168,26.448 L56.112,9.456 L52.128,9.456 L45,26.448 L49.464,26.448 Z M56.16,19.824 L51.888,19.824 L54.048,13.992 L56.16,19.824 Z M75.288,26.448 L75.288,22.872 L68.712,22.872 L68.712,9.456 L64.584,9.456 L64.584,26.448 L75.288,26.448 Z M84.336,26.448 L90.888,9.456 L86.352,9.456 L82.44,21.504 L82.344,21.504 L78.408,9.456 L73.8,9.456 L80.232,26.448 L84.336,26.448 Z M96.408,26.448 L96.408,9.456 L92.28,9.456 L92.28,26.448 L96.408,26.448 Z M103.896,26.448 L103.8,15.336 L103.872,15.336 L110.712,26.448 L115.368,26.448 L115.368,9.456 L111.384,9.456 L111.48,20.544 L111.408,20.544 L104.592,9.456 L99.912,9.456 L99.912,26.448 L103.896,26.448 Z M133.416,26.88 C134.728,26.88 135.94,26.744 137.052,26.472 C138.164,26.2 139.136,25.848 139.968,25.416 L139.968,25.416 L139.968,16.416 L132.96,16.416 L132.96,19.728 L136.2,19.728 L136.2,22.824 C135.832,22.984 135.432,23.1 135,23.172 C134.568,23.244 134.112,23.28 133.632,23.28 C132.8,23.28 132.06,23.148 131.412,22.884 C130.764,22.62 130.22,22.252 129.78,21.78 C129.34,21.308 129.008,20.744 128.784,20.088 C128.56,19.432 128.448,18.712 128.448,17.928 C128.448,17.16 128.572,16.452 128.82,15.804 C129.068,15.156 129.416,14.596 129.864,14.124 C130.312,13.652 130.84,13.284 131.448,13.02 C132.056,12.756 132.72,12.624 133.44,12.624 C134.272,12.624 135.016,12.772 135.672,13.068 C136.328,13.364 136.856,13.744 137.256,14.208 L137.256,14.208 L139.848,11.256 C139.128,10.568 138.192,10.02 137.04,9.612 C135.888,9.204 134.64,9 133.296,9 C132,9 130.792,9.208 129.672,9.624 C128.552,10.04 127.576,10.636 126.744,11.412 C125.912,12.188 125.26,13.128 124.788,14.232 C124.316,15.336 124.08,16.568 124.08,17.928 C124.08,19.272 124.312,20.492 124.776,21.588 C125.24,22.684 125.884,23.624 126.708,24.408 C127.532,25.192 128.516,25.8 129.66,26.232 C130.804,26.664 132.056,26.88 133.416,26.88 Z M147.36,26.448 L147.36,19.704 L148.752,19.704 L152.424,26.448 L157.224,26.448 L152.784,19.176 C153.888,18.84 154.752,18.28 155.376,17.496 C156,16.712 156.312,15.744 156.312,14.592 C156.312,13.616 156.128,12.8 155.76,12.144 C155.392,11.488 154.9,10.96 154.284,10.56 C153.668,10.16 152.972,9.876 152.196,9.708 C151.42,9.54 150.624,9.456 149.808,9.456 L149.808,9.456 L143.328,9.456 L143.328,26.448 L147.36,26.448 Z M149.28,16.728 L147.336,16.728 L147.336,12.768 L149.52,12.768 C149.824,12.768 150.14,12.792 150.468,12.84 C150.796,12.888 151.092,12.98 151.356,13.116 C151.62,13.252 151.836,13.444 152.004,13.692 C152.172,13.94 152.256,14.264 152.256,14.664 C152.256,15.096 152.164,15.448 151.98,15.72 C151.796,15.992 151.56,16.2 151.272,16.344 C150.984,16.488 150.664,16.588 150.312,16.644 C149.96,16.7 149.616,16.728 149.28,16.728 L149.28,16.728 Z M171.096,26.448 L171.096,22.944 L163.2,22.944 L163.2,19.416 L170.256,19.416 L170.256,16.128 L163.2,16.128 L163.2,12.936 L170.664,12.936 L170.664,9.456 L159.24,9.456 L159.24,26.448 L171.096,26.448 Z M182.064,26.448 L182.064,19.248 L188.592,9.456 L183.792,9.456 L180.168,15.744 L176.544,9.456 L171.576,9.456 L177.96,19.248 L177.96,26.448 L182.064,26.448 Z"
                        id="ALVINGREY"
                        fill="#FFFFFF"
                        fillRule="nonzero"
                      />
                    </g>
                  </g>
                </g>
              </svg>
            </Link>
          </div>
          <Link href="register">
            <button className="action-btn red-bg">Join AG</button>
          </Link>
        </div>
        <div className="hero-banner">
          <div className="container-fluid">
            <div className="hero-inner">
              <div className="row">
                <div className="col-lg-5">
                  <h1 className="hero-text">
                    <span>GROW</span> <span>YOUR</span> <span>REAL</span>
                    <span>ESTATE</span> <span>CAREER</span>
                  </h1>
                  <div className="social-icons">
                    <a href="https://www.facebook.com/Alvin-Grey-and-Associates-118386450010938/">
                      <i className="fab fa-facebook-f" />
                    </a>
                    <a href>
                      <i className="fab fa-twitter" />
                    </a>
                    <a href="https://instagram.com/alvingreyandassociates?utm_medium=copy_link">
                      <i className="fab fa-instagram" />
                    </a>
                    <a href>
                      <i className="fab fa-linkedin-in" />
                    </a>
                    <a href="hello@alvinggrey.com">
                      <i className="fas fa-envelope" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="gridbox content-box rtl">
          <div className="text-box bg-white ltr mb-n-8">
            <h1 className="intro">
              We’re real <br />
              with Realtors
            </h1>
            <p className="story-text">
              The Real Estate space in Nigeria is a competitive terrain for all
              stakeholders, even more so for Realtors. There is no shortage of
              individuals peddling Real Estate offerings with little or no
              distinguishing factors or leverage. The unsavory quest to find the
              best properties to sell for decent commissions has discouraged
              many and dulled their passion to become exceptional Realtors.
              <br />
              <br />
              At Alvin Grey, we have made it our life's work to equip you with
              all the tools you need to thrive in Real Estate and enjoy a
              rewarding career.
              <br />
              <br />
              We have invested resources into identifying the most crucial
              things you need to succeed as a Realtor and we are more than
              willing to arm you with these tools and support your continued
              growth.
            </p>
          </div>
          <div className="image-section">
            <div className="image-box">
              <img src="/Were-Real-with-Realtors.jpg" alt />
            </div>
            <div className="content-box rtl gridbox">
              <div className="carousel-box ltr">
                <div className="carousel-title">
                  <div
                    className={
                      tab == "mission" ? "single-item bar" : "single-item"
                    }
                  >
                    <h4 onClick={() => setTab("mission")}>Mission</h4>
                  </div>
                  <div
                    className={
                      tab == "values" ? "single-item bar" : "single-item"
                    }
                  >
                    <h4 onClick={() => setTab("values")}>Values</h4>
                  </div>
                  <div
                    className={
                      tab == "beliefs" ? "single-item bar" : "single-item"
                    }
                  >
                    <h4 onClick={() => setTab("beliefs")}>Beliefs</h4>
                  </div>
                </div>
                {tab == "mission" ? (
                  <div className="carousel-items">
                    <p>
                      Build <b>sustainable</b> real estate <b>careers</b> marked
                      by high <b>efficiency, effectiveness,</b> and{" "}
                      <b>steady</b> growth
                    </p>
                  </div>
                ) : tab == "beliefs" ? (
                  <div className="carousel-items">
                    <p>
                      Uprightness: be honest and sincere Acumen: make good
                      decisions fast Dedication: dress up, show up, everyday
                      Tenacity: stay on for as long as it takes Service: serve
                      your clients, serve other Greys Excellence: in all things
                      Empathy: do what is best for others Fairness: everybody
                      wins Improvise: make the best of every situation Results:
                      be driven by output
                    </p>
                  </div>
                ) : (
                  <div className="carousel-items">
                    <p>
                    We believe in <b>Hard work</b>, <b>integrity</b>, <b>empathy</b> and <b>tenacity</b>.
                    </p>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="new-section">
          <div className="testimonial-container">
            <div className="image-sec">
              <img src="/alvina.svg" alt />
            </div>
            <h1 className="test">
              "<b>AG</b> gives me the right kind of exposure and tools I needed
              to jumpstart my <b>Real Estate career</b> and continue to exceed
              even my own expectations. I have been able to hone my sales skills
              and close deals with <b>higher success rates</b> thanks to AG. "
            </h1>
            <p className="quotes">Adesuwa Rita Abiode</p>
            <p className="quotes">Alvin Grey Associate</p>
            <p className="quotes">Lagos, Nigeria</p>
          </div>
        </div>
        <div className="gridbox content-box ltr">
          <div className="text-box ltr mt-4r">
            <h1 className="intro">
              We’ll Show You <br />
              The Money
            </h1>
            <p className="story-text">
              Your Real Estate career should be very rewarding and that's plain
              and simple. We believe in a win-win philosophy that ensures that
              you get what you've earned promptly and in full.
              <br />
              <br />
              At AG, we do not just run a multi-level marketing structure, we
              build a network of friends, family, and colleagues who work
              together towards a common goal. Our structure allows you to grow
              while also enabling people around you to grow as well when they
              become your downlines. Being an AG Realtor is fair, equitable and
              everybody wins.
            </p>
            {/* <div className="reads">
        <div className="arrow">
          <div className="arrow-right">
            <img src alt />
          </div>
        </div>
        <div className="readmore">
          <p className="headline mb-0">Learn:</p>
          <p className="title">
            Keller Williams Ends 2020 With Most Successful Fourth Quarter Ever
          </p>
        </div>
      </div>
      <img src alt className="read" /> */}
          </div>
          <div className="image-section">
            <div className="image-boxx">
              <img src="/wewillshow-you-themoney.jpg" alt />
            </div>
            <div className="content-box ltr gridbox">
              <div className="item-box">
                <h1>Commissions:</h1>
                <div className="item-inner">
                  <div className="item-single">
                    <h2>Up to 5%</h2>
                    <p>On Direct Sales</p>
                  </div>
                  <div className="item-single">
                    <h2>1%</h2>
                    <p>On Indirect Sales</p>
                  </div>
                  <div className="item-single">
                    <h2>5 Downlines</h2>
                    <p>Qualifies you for incentives</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="new-section">
          <div className="testimonial-container">
            <div className="image-sec">
              <img src="/alvina.svg" alt />
            </div>
            <h1 className="test">
              "Thank you LandWey for setting the pace and for going the extra
              mile. For the first time, I got teary about this job literally, a
              new light has come through and I am proud of this job. I am even
              more proud to be a LandWey realtor"
            </h1>
            <p className="quotes">Empire Homes Realty</p>
            <p className="quotes">Lagos, Nigeria</p>
          </div>
        </div>
        <div className="gridbox content-box rtl light-bg">
          <div className="text-box ltr mt-4r">
            <h1 className="intro">
              A Real Estate Career with <br />
              Bells &amp; Whistles
            </h1>
            <p className="story-text">
              Incentives are not new to Realtors; the Real Estate industry is
              inundated with all manner of gifts aimed at motivating its
              stakeholders but at AG, we think of motivation differently. We are
              more interested in sustainable growth. We arm our Realtors with
              bleeding-edge technology tools, on-the-job training, and
              leadership development to empower you to live an overall better
              life.
              <br />
              <br />
              Of course, we give gifts too. With Alvin Grey, you enjoy a free
              workspace with the internet, low-interest loans, car, and cash
              rewards, and company-sponsored vacations, trips, and events.
            </p>
          </div>
          <div className="image-section">
            <div class="image-boxx">
              <img src="/Bells-&-Whistles.jpg" alt="" />
            </div>
            <div className="content-box ltr gridbox">
              <div className="item-box col4">
                {/* <h1>In the Keller Cloud, to date:</h1> */}
                <div className="item-inner">
                  <div className="item-single">
                    <h2>100</h2>
                    <p>Fully equipped workspace slots</p>
                  </div>
                  <div className="item-single">
                    <h2>Over 100</h2>
                    <p>Car Gifts</p>
                  </div>
                  <div className="item-single">
                    <h2>Over 50</h2>
                    <p>Sponsored international vacations</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="new-section py-5">
          <div className="testimonial-container">
            <div className="image-sec">
              <img src="/alvino.svg" alt />
            </div>
            <h1 className="test">
              I am so emotional right now. THANK YOU for who you are and the
              high standards and speed you have brought into the Lagos real
              estate market…have I mentioned the top notch amazing trainings?
              The crisp professionalism of staff? Amazing!
            </h1>
            <p className="quotes">Munachi Arinze</p>
            <p className="quotes">Lagos, Nigeria</p>
          </div>
        </div>
        <div className="mega">
          <div className="contains">
            <h2>Meet the Greys</h2>
            <p>
              very day, top-tier agents are turning heads as they
              <b>make the move</b> to Keller Williams. <br />
              Here's a look at a few of who are now proud to call KW home!
            </p>
            <Link href="/register">
              <button className="action-btn reversed">Join AG</button>
            </Link>
            <div className="mega-pic-a left">
              <img
                src="https://outfront.kw.com/wp-content/uploads/2021/05/KathyHigginbotham_OF.jpg"
                alt
              />
            </div>
            <div className="mega-pic-a">
              <img src='/1.jpg' alt />
            </div>
            <div className="mega-pic-a">
              <img src='/2.jpg' alt />
            </div>
            <div className="mega-pic-a left">
              <img src='/3.jpg' alt />
            </div>
            <div className="mega-pic-a left">
              <img src='/4.jpg' alt />
            </div>
            <div className="mega-pic-a">
              <img src='/5.jpg' alt />
            </div>
            <div className="mega-pic-a left">
              <img src='/6.jpg' alt />
            </div>
            <div className="mega-pic-a">
              <img src='/7.jpg' alt />
            </div>
            <div className="mega-pic-a left">
              <img src='/8.jpg' alt />
            </div>
            <div className="mega-pic-a">
              <img src='/9.jpg' alt />
            </div>
          </div>
        </div>
        <div className="grey-bg w-100">
          <div className="gridbox content-box ltr md-container">
            <div className="text-box mt-n-04 ltr">
              <h1 className="intro">
                We’re talking <br />
                about a
                <br />
                Real-volution
              </h1>
            </div>
            <div className="col-span-6 d-sm-none d-md-block">&nbsp;</div>
            <div className="col-span-4 key-section">
              <h3 className="head-text">Remote Inspection</h3>
              <p className="story-text">
                Events happening around the globe have necessitated a more
                deliberate adoption of technology in every industry and sector.
                Limitations on physical presence have opened people’s minds to
                possibilities in Virtual Reality and other relevant technology.
                Take full advantage of AG’s remote inspection tools and close
                deals from halfway around the world.
              </p>
              {/* <div className="reads">
          <div className="arrow">
            <div className="arrow-right">
              <img src alt />
            </div>
          </div>
          <div className="readmore">
            <p className="headline mb-0">Read on Outfront:</p>
            <p className="title">
              Keller Williams Ends 2020 With Most Successful Fourth Quarter
              Ever
            </p>
          </div>
        </div> */}
            </div>
            <div className="col-span-4 key-section mid">
              <h3 className="head-text">The AG Web App</h3>
              <p className="story-text">
                The Alvin Grey RealtorCenter Web Application gives you the tools
                you need to onboard new downlines, manage your clients, track
                and claim your commissions, market your properties and showcase
                your achievements and testimonials. It is the virtual office you
                need to supercharge your Realtor business.
              </p>
              {/* <div className="reads">
          <div className="arrow">
            <div className="arrow-right">
              <img src alt />
            </div>
          </div>
          <div className="readmore">
            <p className="headline mb-0">Read on Outfront:</p>
            <p className="title">
              Keller Williams Ends 2020 With Most Successful Fourth Quarter
              Ever
            </p>
          </div>
        </div> */}
            </div>
            <div className="col-span-4 key-section">
              <h3 className="head-text">Media Tools &amp; Tips</h3>
              <p className="story-text">
                Learn how to get the online attention you need to find
                prospective clients and drive up your sales. Our partner
                platform, Learn by Oxygen, provides digital content that you can
                access right from the RealtorCenter application on a wide
                variety of topics focused on digital media.
              </p>
              {/* <div className="reads">
          <div className="arrow">
            <div className="arrow-right">
              <img src alt />
            </div>
          </div>
          <div className="readmore">
            <p className="headline mb-0">Read on Outfront:</p>
            <p className="title">
              Keller Williams Ends 2020 With Most Successful Fourth Quarter
              Ever
            </p>
          </div>
        </div> */}
            </div>
          </div>
          <div className="center-btn">
            <Link href="/technology">
              <button className="action-btn learn">
                Learn more about our tech
              </button>
            </Link>
          </div>
        </div>
        {/* <div class="gridbox content-box ltr">
			<div class="text-box ltr">
				<h1 class="intro mt-4r">Strength in <br />Numbers</h1>
				<p class="story-text">
					Simply put, we're the best real estate company. Why? Because we
					measure our success by your success, letting the numbers speak for
					themselves. Keller Williams is the number one real estate franchise by
					agent count, closed units, and closed sales volume in the United
					States.
					<br />
					<br />
					There is strength in numbers. So, in the face of a shifting market or
					industry disruption, make the choice to take a stand – on solid ground
					– with the number one real estate franchise company. We have what you
					need to do more and be more.
				</p>

				<div class="reads">
					<div class="arrow">
						<div class="arrow-right">
							<img src="" alt="" />
						</div>
					</div>
					<div class="readmore">
						<p class="headline mb-0">Read on Outfront:</p>
						<p class="title">
							Keller Williams Ends 2020 With Most Successful Fourth Quarter Ever
						</p>
					</div>
				</div>
				<img src="" alt="" class="read" />
			</div>
			<div class="image-section">
				<div class="image-boxx">
					<img src="" alt="" />
				</div>
				<div class="content-box ltr gridbox">
					<div class="item-box col-span-6 col-start-6">
						<h1>In 2020:</h1>
						<div class="item-inner">
							<div class="item-single">
								<h2>187,000</h2>
								<p>Associates Globally</p>
							</div>
							<div class="item-single">
								<h2>1,222,377</h2>
								<p>U.S. & Canada Transaction</p>
							</div>
							<div class="item-single">
								<h2>$407B</h2>
								<p>U.S. & Canada Sales Volume</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="new-section">
			<div class="testimonial-container">
				<div class="image-sec"><img src="" alt="" /></div>
				<h1 class="test">
					"When it comes to supporting team leaders who are building and
					nurturing their agents, Keller Williams has impressed us with their
					models, training, education, and their innovative attention to future
					technologies."
				</h1>
				<p class="quotes">MIKE MCCANN, THE MIKE MCCANN TEAM</p>
				<p class="quotes">PHILADELPHIA, PA</p>
				<p class="quotes"><b>$365M</b> SALES VOLUME</p>
			</div>
		</div>
		<div class="gridbox content-box rtl">
			<div class="text-box ltr">
				<h1 class="intro mt-4r">Strength in <br />Numbers</h1>
				<p class="story-text">
					Simply put, we're the best real estate company. Why? Because we
					measure our success by your success, letting the numbers speak for
					themselves. Keller Williams is the number one real estate franchise by
					agent count, closed units, and closed sales volume in the United
					States.
					<br />
					<br />
					There is strength in numbers. So, in the face of a shifting market or
					industry disruption, make the choice to take a stand – on solid ground
					– with the number one real estate franchise company. We have what you
					need to do more and be more.
				</p>

				<div class="reads">
					<div class="arrow">
						<div class="arrow-right">
							<img src="" alt="" />
						</div>
					</div>
					<div class="readmore">
						<p class="headline mb-0">Read on Outfront:</p>
						<p class="title">
							Keller Williams Ends 2020 With Most Successful Fourth Quarter Ever
						</p>
					</div>
				</div>
				<div class="reads">
					<div class="arrow">
						<div class="arrow-right">
							<img src="" alt="" />
						</div>
					</div>
					<div class="readmore">
						<p class="headline mb-0">Read on Outfront:</p>
						<p class="title">
							Keller Williams Ends 2020 With Most Successful Fourth Quarter Ever
						</p>
					</div>
				</div>
			</div>
			<div class="image-section">
				<div class="image-boxx">
					<img src="" alt="" />
				</div>
				<div class="ltr gridbox">
					<div class="small-textbox col-span-5 col-start-2 mt-n-20">
						<h3>Over <b> $1.5 Billion</b> Shared</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="new-section mt-5">
			<div class="testimonial-container">
				<div class="image-sec"><img src="" alt="" /></div>
				<h1 class="test">
					"Keller Williams believes that <b>agents are stakeholders </b>in the
					success of the market center and therefore should share in decision
					making. Together, they [top agents and office leadership]
					<b>jointly make office decisions</b> relating to financial planning,
					recruiting and retention, marketing and advertising, training,
					technology, social events, and philanthropic giving."
				</h1>
				<p class="quotes">MIKE MCCANN, THE MIKE MCCANN TEAM</p>
				<p class="quotes">PHILADELPHIA, PA</p>
				<p class="quotes"><b>$365M</b> SALES VOLUME</p>
			</div>
		</div>
		<div class="gridbox content-box ltr">
			<div class="text-box ltr">
				<h1 class="intro mt-4r">Expand Your <br />Reach</h1>
				<p class="story-text">
					The KW value model serves as a comprehensive framework that allows
					well-run businesses to create even bigger opportunities by scaling and
					building a dream team. Reach new markets, then cross state and
					international borders, and expand beyond what you once thought
					possible in your real estate career.
				</p>

				<div class="reads">
					<div class="arrow">
						<div class="arrow-right">
							<img src="" alt="" />
						</div>
					</div>
					<div class="readmore">
						<p class="headline mb-0">Read on Outfront:</p>
						<p class="title">
							Keller Williams Ends 2020 With Most Successful Fourth Quarter Ever
						</p>
					</div>
				</div>
			</div>
			<div class="image-section">
				<div class="image-boxx">
					<img src="" alt="" />
				</div>
			</div>
		</div> */}
        {/* <div class="new-section mt-5">
			<div class="testimonial-container">
				<div class="image-sec"><img src="" alt="" /></div>
				<h1 class="test">
					"I really wanted to have a place where I can
					<b>build a legacy for my family</b> and a platform that I can really
					grow with my team."
				</h1>
				<p class="quotes">MIKE MCCANN, THE MIKE MCCANN TEAM</p>
				<p class="quotes">PHILADELPHIA, PA</p>
				<p class="quotes"><b>$365M</b> SALES VOLUME</p>
			</div>
		</div> */}
        <div className="first-footer">
          <div className="footer-content">
            <h1 className="foot-text">
              Realtor-first . Tech-enabled . Result-oriented
            </h1>
            <p className="story-text text-white mb-4">
              Become an Alvin Grey Realtor today and be a part of a new
              generation doing things differently, <br />
              getting better results, and living better quality lives.
              <br />
              Everything is ready and we can't wait to have you on board.
            </p>
            <button className="action-btn reverse">Join AG</button>
          </div>
        </div>

        <Footer />
        {/* Javascript  */}
      </div>
    </div>
  );
}
