import Link from "next/link";
import React, { useState } from "react";
import Footer from "../components/Navigation/Footer";
import TopNav from "../components/Navigation/TopNav";
import RegisterationPropmpt from "../components/RegisterationPropmpt";
import countries from "../countries.json";
import Fetch from "../utilities/Fetch";
import Modal from "../utilities/Modal";
import Spinner from "../utilities/Spinner";

export default function register() {
  console.log(process.env.NEXT_PUBLIC_APIBASEURl);
  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [step, setStep] = useState("a");
  const [code, setCode] = useState("");
  const [user, setUser] = useState({
    email: "",
    password: "",
    firstName: "",
    lastName: "",
    phoneNumber: "",
    usersName: "",
    country: "",
    accountNumber: "",
    accountName: "",
    bankName: "",
    media: {
      name: "",
      extention: "",
      base64String: "",
      propertyId: 0,
      isImage: true,
      isVideo: true,
      isDocument: true,
    },
  });

  const handleUserChange = (e) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
    console.log(user);
  };

  const onFileChange = (e) => {
    const file = e.target.files[0];
    console.log(file);
    const reader = new FileReader();
    reader.onabort = () => {
      console.log("Errrrrrrrrrrrrrooooooooooorrrrrrr");
    };
    reader.onerror = () => {
      console.log("Errrrrrrrrrrrrrooooooooooorrrrrrr");
    };
    reader.onload = () => {
      // Do whatever you want with the file contents
      const binaryStr = reader.result.split(",")[1];
      console.log(reader.result);
      //console.log(binaryStr);
      console.log(binaryStr);
      composeMedia(binaryStr, file);
    };
    console.log(file);
    reader.readAsDataURL(file);
  };

  const composeMedia = (bytes, file) => {
    var newMedia = {
      name: file.name,
      extention: getFileExtention(file.name),
      base64String: bytes,
      propertyId: 0,
      isImage:
        getFileExtention(file.name) == "jpeg" ||
        getFileExtention(file.name) == "jpg" || getFileExtention(file.name) == "png"
          ? true
          : false,
      isVideo: getFileExtention(file.name) == "mp4" ? true : false,
      isDocument: getFileExtention(file.name) == "pdf" ? true : false,
    };
    console.log(newMedia);
    setUser({...user,media:newMedia})
  };

  const getFileExtention = (fileName) => {
    return fileName.split(".")[1];
  };

  const submit = async (e) => {
    setLoading(true);
    e.preventDefault();
    console.log(user);
    const data = await Fetch("user/register", "post", user);
    if (!data.status) {
      setErrorMessage(data.message);
      return;
    }
    setStep("b");
    setLoading(false);
    // setIsOpen(true);
  };

  const verify = async (e) => {
    setLoading(true);
    e.preventDefault();
    try {
      const data = await Fetch(`user/verifyuser/${code}/${user.email}`);
      if (!data.status) {
        setErrorMessage(data.message);
        return;
      }

      setIsOpen(true);
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <TopNav />
      <div className="menu">
        <div className="logo">
          <Link href="/">
            <svg
              width="198px"
              height="35px"
              viewBox="0 0 198 35"
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
            >
              <title>Full Logo-White Background@1x</title>
              <g
                id="Page-1"
                stroke="none"
                strokeWidth={1}
                fill="none"
                fillRule="evenodd"
              >
                <g id="A4" transform="translate(-273.000000, -398.000000)">
                  <g
                    id="Full-Logo-White-Background"
                    transform="translate(273.000000, 398.000000)"
                  >
                    <rect
                      id="Rectangle"
                      fill="#000000"
                      x={0}
                      y={0}
                      width={8}
                      height={35}
                    />
                    <rect
                      id="Rectangle"
                      fill="#000000"
                      x={12}
                      y={0}
                      width={8}
                      height={35}
                    />
                    <rect
                      id="Rectangle"
                      fill="#000000"
                      x={24}
                      y={0}
                      width={8}
                      height={35}
                    />
                    <rect
                      id="Rectangle"
                      fill="#CD1A1D"
                      x={36}
                      y={0}
                      width={162}
                      height={35}
                    />
                    <path
                      d="M49.464,26.448 L50.712,23.112 L57.288,23.112 L58.608,26.448 L63.168,26.448 L56.112,9.456 L52.128,9.456 L45,26.448 L49.464,26.448 Z M56.16,19.824 L51.888,19.824 L54.048,13.992 L56.16,19.824 Z M75.288,26.448 L75.288,22.872 L68.712,22.872 L68.712,9.456 L64.584,9.456 L64.584,26.448 L75.288,26.448 Z M84.336,26.448 L90.888,9.456 L86.352,9.456 L82.44,21.504 L82.344,21.504 L78.408,9.456 L73.8,9.456 L80.232,26.448 L84.336,26.448 Z M96.408,26.448 L96.408,9.456 L92.28,9.456 L92.28,26.448 L96.408,26.448 Z M103.896,26.448 L103.8,15.336 L103.872,15.336 L110.712,26.448 L115.368,26.448 L115.368,9.456 L111.384,9.456 L111.48,20.544 L111.408,20.544 L104.592,9.456 L99.912,9.456 L99.912,26.448 L103.896,26.448 Z M133.416,26.88 C134.728,26.88 135.94,26.744 137.052,26.472 C138.164,26.2 139.136,25.848 139.968,25.416 L139.968,25.416 L139.968,16.416 L132.96,16.416 L132.96,19.728 L136.2,19.728 L136.2,22.824 C135.832,22.984 135.432,23.1 135,23.172 C134.568,23.244 134.112,23.28 133.632,23.28 C132.8,23.28 132.06,23.148 131.412,22.884 C130.764,22.62 130.22,22.252 129.78,21.78 C129.34,21.308 129.008,20.744 128.784,20.088 C128.56,19.432 128.448,18.712 128.448,17.928 C128.448,17.16 128.572,16.452 128.82,15.804 C129.068,15.156 129.416,14.596 129.864,14.124 C130.312,13.652 130.84,13.284 131.448,13.02 C132.056,12.756 132.72,12.624 133.44,12.624 C134.272,12.624 135.016,12.772 135.672,13.068 C136.328,13.364 136.856,13.744 137.256,14.208 L137.256,14.208 L139.848,11.256 C139.128,10.568 138.192,10.02 137.04,9.612 C135.888,9.204 134.64,9 133.296,9 C132,9 130.792,9.208 129.672,9.624 C128.552,10.04 127.576,10.636 126.744,11.412 C125.912,12.188 125.26,13.128 124.788,14.232 C124.316,15.336 124.08,16.568 124.08,17.928 C124.08,19.272 124.312,20.492 124.776,21.588 C125.24,22.684 125.884,23.624 126.708,24.408 C127.532,25.192 128.516,25.8 129.66,26.232 C130.804,26.664 132.056,26.88 133.416,26.88 Z M147.36,26.448 L147.36,19.704 L148.752,19.704 L152.424,26.448 L157.224,26.448 L152.784,19.176 C153.888,18.84 154.752,18.28 155.376,17.496 C156,16.712 156.312,15.744 156.312,14.592 C156.312,13.616 156.128,12.8 155.76,12.144 C155.392,11.488 154.9,10.96 154.284,10.56 C153.668,10.16 152.972,9.876 152.196,9.708 C151.42,9.54 150.624,9.456 149.808,9.456 L149.808,9.456 L143.328,9.456 L143.328,26.448 L147.36,26.448 Z M149.28,16.728 L147.336,16.728 L147.336,12.768 L149.52,12.768 C149.824,12.768 150.14,12.792 150.468,12.84 C150.796,12.888 151.092,12.98 151.356,13.116 C151.62,13.252 151.836,13.444 152.004,13.692 C152.172,13.94 152.256,14.264 152.256,14.664 C152.256,15.096 152.164,15.448 151.98,15.72 C151.796,15.992 151.56,16.2 151.272,16.344 C150.984,16.488 150.664,16.588 150.312,16.644 C149.96,16.7 149.616,16.728 149.28,16.728 L149.28,16.728 Z M171.096,26.448 L171.096,22.944 L163.2,22.944 L163.2,19.416 L170.256,19.416 L170.256,16.128 L163.2,16.128 L163.2,12.936 L170.664,12.936 L170.664,9.456 L159.24,9.456 L159.24,26.448 L171.096,26.448 Z M182.064,26.448 L182.064,19.248 L188.592,9.456 L183.792,9.456 L180.168,15.744 L176.544,9.456 L171.576,9.456 L177.96,19.248 L177.96,26.448 L182.064,26.448 Z"
                      id="ALVINGREY"
                      fill="#FFFFFF"
                      fillRule="nonzero"
                    />
                  </g>
                </g>
              </g>
            </svg>
          </Link>
        </div>
        <Link href="register">
          <button className="action-btn red-bg">Join AG</button>
        </Link>
      </div>
      <Modal
        open={isOpen}
        // onClose={() => {
        //   setIsOpen(false);
        // }}
      >
        <RegisterationPropmpt />
      </Modal>
      <div className="sign-grid">
        <div className="sign-image">
          <img src="/Join_AG_page.jpeg" alt />
        </div>
        <div className="sign-form">
          <h2 className="contactus text-black">Join AG</h2>
          {step == "a" ? (
            <form onSubmit={submit}>
              <div>
                <div>
                  <p classname="text-danger text-center">
                    {errorMessage ? errorMessage : null}
                  </p>
                </div>
                <div className="personal-image">
                  <label className="label">
                    <input type="file" onChange={onFileChange} />
                    <figure className="personal-figure">
                      <img
                        src="/alvina.svg"
                        className="personal-avatar"
                        alt="avatar"
                      />
                      <figcaption className="personal-figcaption">
                        <img src="https://raw.githubusercontent.com/ThiagoLuizNunes/angular-boilerplate/master/src/assets/imgs/camera-white.png" />
                      </figcaption>
                    </figure>
                  </label>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="input-box">
                    <label className="input-label">Firstname *</label>
                    <input
                      name="firstName"
                      type="text"
                      className="form-input"
                      placeholder="Enter your Name"
                      required
                      onChange={handleUserChange}
                    />
                  </div>
                </div>
                <div className="col">
                  <div className="input-box">
                    <label className="input-label">Lastname *</label>
                    <input
                      name="lastName"
                      type="text"
                      className="form-input"
                      placeholder="Enter your Name"
                      required
                      onChange={handleUserChange}
                    />
                  </div>
                </div>
              </div>
              <div className="input-box">
                <label className="input-label">Email *</label>
                <input
                  name="email"
                  type="email"
                  className="form-input"
                  placeholder="Enter your Email"
                  required
                  onChange={handleUserChange}
                />
              </div>
              <div className="input-box">
                <label className="input-label">Username *</label>
                <input
                  name="usersName"
                  type="text"
                  className="form-input"
                  placeholder="Pick a username"
                  required
                  onChange={handleUserChange}
                />
              </div>
              <div className="input-box">
                <label className="input-label" type="number">
                  Phone Number *
                </label>
                <input
                  name="phoneNumber"
                  type="tel"
                  className="form-input"
                  placeholder="Enter your Phone number"
                  required
                  onChange={handleUserChange}
                />
              </div>
              <div className="input-box">
                <label className="input-label">Password *</label>
                <input
                  name="password"
                  type="password"
                  className="form-input"
                  placeholder="Enter a strong Password"
                  required
                  onChange={handleUserChange}
                />
              </div>
              <div className="input-box">
                <label className="input-label">Repeat Password *</label>
                <input
                  type="password"
                  className="form-input"
                  placeholder="Repeat the password above"
                />
              </div>
              <div className="input-box">
                <label className="input-label">Country *</label>
                <select
                  className="form-input"
                  required
                  name="country"
                  onChange={handleUserChange}
                >
                  <option>Please Select a country</option>
                  {countries.map((country, index) => {
                    return (
                      <option value={country.name} key={index}>
                        {country.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="row">
                <div className="col">
                  <div className="input-box">
                    <label className="input-label">Bank Name *</label>
                    <input
                      name="bankName"
                      type="text"
                      className="form-input"
                      placeholder="Enter your Name"
                      required
                      onChange={handleUserChange}
                    />
                  </div>
                </div>
                <div className="col">
                  <div className="input-box">
                    <label className="input-label">Account Number *</label>
                    <input
                      name="accountNumber"
                      type="text"
                      className="form-input"
                      placeholder="Enter your Name"
                      required
                      onChange={handleUserChange}
                    />
                  </div>
                </div>
                <div className="col">
                  <div className="input-box">
                    <label className="input-label">Account Name *</label>
                    <input
                      name="accountName"
                      type="text"
                      className="form-input"
                      placeholder="Enter your Name"
                      required
                      onChange={handleUserChange}
                    />
                  </div>
                </div>
              </div>
              <input type="checkbox" className="frorm-input mr-1" required />
              <label>
                By checking this box you acknoledge to accepting the{" "}
                <Link href="/terms.pdf">terms and conditon.</Link>
              </label>
              <button
                type="submit"
                className="action-btn red-bgg mx-auto w-100 mt-2"
              >
                {loading ? <Spinner /> : "Sign Up"}
              </button>
            </form>
          ) : (
            <div>
              <p className="text-center">
                {" "}
                Please enter the code sent to your email here to verify your
                email address
              </p>
              <div className="input-box">
                <label className="input-label">Verification Code</label>
                <input
                  name="code"
                  type="text"
                  className="form-input"
                  placeholder="Enter the verification code here"
                  required
                  onChange={(e) => setCode(e.target.value)}
                />
              </div>
              <button
                className="action-btn red-bgg mx-auto w-100 mt-2"
                onClick={async (e) => await verify(e)}
              >
                {loading ? <Spinner /> : "Verify"}
              </button>
            </div>
          )}
        </div>
      </div>
      <Footer />
      {/* Javascript  */}
    </div>
  );
}
