import React from "react";
import TopNav from "../components/Navigation/TopNav";

export default function profile() {
  return (
    <div>
      <TopNav />
      <div>
        <div className="menu red">
          <div className="logo">
            <svg
              width="198px"
              height="35px"
              viewBox="0 0 198 35"
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
            >
              <title>Full Logo-White Background@1x</title>
              <g
                id="Page-1"
                stroke="none"
                strokeWidth={1}
                fill="none"
                fillRule="evenodd"
              >
                <g id="A4" transform="translate(-273.000000, -398.000000)">
                  <g
                    id="Full-Logo-White-Background"
                    transform="translate(273.000000, 398.000000)"
                  >
                    <rect
                      id="Rectangle"
                      fill="#000000"
                      x={0}
                      y={0}
                      width={8}
                      height={35}
                    />
                    <rect
                      id="Rectangle"
                      fill="#000000"
                      x={12}
                      y={0}
                      width={8}
                      height={35}
                    />
                    <rect
                      id="Rectangle"
                      fill="#000000"
                      x={24}
                      y={0}
                      width={8}
                      height={35}
                    />
                    <rect
                      id="Rectangle"
                      fill="#CD1A1D"
                      x={36}
                      y={0}
                      width={162}
                      height={35}
                    />
                    <path
                      d="M49.464,26.448 L50.712,23.112 L57.288,23.112 L58.608,26.448 L63.168,26.448 L56.112,9.456 L52.128,9.456 L45,26.448 L49.464,26.448 Z M56.16,19.824 L51.888,19.824 L54.048,13.992 L56.16,19.824 Z M75.288,26.448 L75.288,22.872 L68.712,22.872 L68.712,9.456 L64.584,9.456 L64.584,26.448 L75.288,26.448 Z M84.336,26.448 L90.888,9.456 L86.352,9.456 L82.44,21.504 L82.344,21.504 L78.408,9.456 L73.8,9.456 L80.232,26.448 L84.336,26.448 Z M96.408,26.448 L96.408,9.456 L92.28,9.456 L92.28,26.448 L96.408,26.448 Z M103.896,26.448 L103.8,15.336 L103.872,15.336 L110.712,26.448 L115.368,26.448 L115.368,9.456 L111.384,9.456 L111.48,20.544 L111.408,20.544 L104.592,9.456 L99.912,9.456 L99.912,26.448 L103.896,26.448 Z M133.416,26.88 C134.728,26.88 135.94,26.744 137.052,26.472 C138.164,26.2 139.136,25.848 139.968,25.416 L139.968,25.416 L139.968,16.416 L132.96,16.416 L132.96,19.728 L136.2,19.728 L136.2,22.824 C135.832,22.984 135.432,23.1 135,23.172 C134.568,23.244 134.112,23.28 133.632,23.28 C132.8,23.28 132.06,23.148 131.412,22.884 C130.764,22.62 130.22,22.252 129.78,21.78 C129.34,21.308 129.008,20.744 128.784,20.088 C128.56,19.432 128.448,18.712 128.448,17.928 C128.448,17.16 128.572,16.452 128.82,15.804 C129.068,15.156 129.416,14.596 129.864,14.124 C130.312,13.652 130.84,13.284 131.448,13.02 C132.056,12.756 132.72,12.624 133.44,12.624 C134.272,12.624 135.016,12.772 135.672,13.068 C136.328,13.364 136.856,13.744 137.256,14.208 L137.256,14.208 L139.848,11.256 C139.128,10.568 138.192,10.02 137.04,9.612 C135.888,9.204 134.64,9 133.296,9 C132,9 130.792,9.208 129.672,9.624 C128.552,10.04 127.576,10.636 126.744,11.412 C125.912,12.188 125.26,13.128 124.788,14.232 C124.316,15.336 124.08,16.568 124.08,17.928 C124.08,19.272 124.312,20.492 124.776,21.588 C125.24,22.684 125.884,23.624 126.708,24.408 C127.532,25.192 128.516,25.8 129.66,26.232 C130.804,26.664 132.056,26.88 133.416,26.88 Z M147.36,26.448 L147.36,19.704 L148.752,19.704 L152.424,26.448 L157.224,26.448 L152.784,19.176 C153.888,18.84 154.752,18.28 155.376,17.496 C156,16.712 156.312,15.744 156.312,14.592 C156.312,13.616 156.128,12.8 155.76,12.144 C155.392,11.488 154.9,10.96 154.284,10.56 C153.668,10.16 152.972,9.876 152.196,9.708 C151.42,9.54 150.624,9.456 149.808,9.456 L149.808,9.456 L143.328,9.456 L143.328,26.448 L147.36,26.448 Z M149.28,16.728 L147.336,16.728 L147.336,12.768 L149.52,12.768 C149.824,12.768 150.14,12.792 150.468,12.84 C150.796,12.888 151.092,12.98 151.356,13.116 C151.62,13.252 151.836,13.444 152.004,13.692 C152.172,13.94 152.256,14.264 152.256,14.664 C152.256,15.096 152.164,15.448 151.98,15.72 C151.796,15.992 151.56,16.2 151.272,16.344 C150.984,16.488 150.664,16.588 150.312,16.644 C149.96,16.7 149.616,16.728 149.28,16.728 L149.28,16.728 Z M171.096,26.448 L171.096,22.944 L163.2,22.944 L163.2,19.416 L170.256,19.416 L170.256,16.128 L163.2,16.128 L163.2,12.936 L170.664,12.936 L170.664,9.456 L159.24,9.456 L159.24,26.448 L171.096,26.448 Z M182.064,26.448 L182.064,19.248 L188.592,9.456 L183.792,9.456 L180.168,15.744 L176.544,9.456 L171.576,9.456 L177.96,19.248 L177.96,26.448 L182.064,26.448 Z"
                      id="ALVINGREY"
                      fill="#FFFFFF"
                      fillRule="nonzero"
                    />
                  </g>
                </g>
              </g>
            </svg>
          </div>
          <div className="profile-logged">
            <button className="action-btn red-bg">
              <div className="user-avatar">
                <img src="/Alvina.svg" alt />
              </div>
              Emeka johnson
            </button>
            <div className="dropdown-bg">
              <ul>
                <li>Logout</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="container-fluid mtp">
          <div className="row">
            <div className="col-lg-3">
              <div className="profile-img">
                <div className="user-img">
                  <img src="/Alvino.svg" alt />
                </div>
              </div>
            </div>
            <div className="col-lg-7">
              <div className="sign-form">
                <h2 className="contactus text-black text-center">
                  Your Profile
                </h2>
                <form>
                  <div className="input-box">
                    <label className="input-label">Email *</label>
                    <input
                      type="text"
                      className="form-input"
                      placeholder="Enter your Email"
                      defaultValue="Emeka johnson"
                      disabled
                    />
                  </div>
                  <div className="input-box">
                    <label className="input-label">Name *</label>
                    <input
                      type="text"
                      className="form-input"
                      placeholder="Enter your Name"
                    />
                  </div>
                  <div className="input-box">
                    <label className="input-label">Phone Number *</label>
                    <input
                      type="text"
                      className="form-input"
                      placeholder="Enter your Phone number"
                    />
                  </div>
                  <div className="input-box">
                    <label className="input-label">Password *</label>
                    <input
                      type="password"
                      className="form-input"
                      placeholder="Enter your Email"
                    />
                  </div>
                  <div className="input-box">
                    <label className="input-label">Repeat Password *</label>
                    <input
                      type="password"
                      className="form-input"
                      placeholder="Enter your password"
                    />
                  </div>
                  <button className="action-btn red-bgg ml-auto mt-5">
                    Update Profile Info
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
