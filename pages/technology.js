import React from "react";
import Footer from "../components/Navigation/Footer";
import TopNav from "../components/Navigation/TopNav";

export default function technology() {
  return (
    <div>
      <body>
        <TopNav />
        <div>
          <div className="menu py-3">
            <div className="left-menu">
              <div className="logo">
                <img src='/logomark_red.png' alt/>
              </div>
              <div className="section-text">Technology</div>
            </div>
            <input type="checkbox" id="hmb" />
            <label htmlFor="hmb">
              <div className="hamburger">
                <div className="hamburg tech" />
              </div>
            </label>
            {/* <div className="right-menu">
              <ul className="right-links tech">
                <li>
                  <a href>Agent</a>
                </li>
                <li>
                  <a href>Consumer</a>
                </li>
                <li>
                  <a href>Team</a>
                </li>
                <li>
                  <a href>Service</a>
                </li>
                <li>
                  <a href>Join</a>
                </li>
              </ul>
            </div> */}
          </div>
          <div className="tech-wrapper">
            <h1 className="headline">Leverage Technology to Scale Your Business</h1>
          </div>
          <div className="tech-body">
            <h2 className="divide-section">An Experience Like None Other</h2>
            <p className="div-section">
            As the lines become blurred between home and work, and the world slowly opens up to normal physical activity, you need a partner that can help you with the right technology to power your business.

            </p>
            <p className="div-section">
            At Alvin Grey, we constantly position ourselves as that viable partner presenting you with the right tools, tech, and resources to run your realtor business without getting overwhelmed and being able to focus on other things that matter to you.

            </p>
          </div>
          <div className="black-section">
            <h4 className="black-intro">End-to-end is just the begining</h4>
            <div className="container-fluid">
              <div className="row px-5">
                <div className="col-lg-6">
                  <div className="black-img">
                    <img
                      src="/371906220_TYPING_ON_LAPTOP_400px.gif"
                      alt
                      style={{ maxWidth: 1024 }}
                    />
                  </div>
                </div>
                <div className="col-lg-6 d-flex justify-content-center flex-column">
                  <p className="div-section text-white text-lg-left">
                    Real estate is about relationships, yet when technology was
                    added to the equation, sustaining the quality of those
                    relationships became more challenging. So, we pivoted toward
                    co-developing technology with our real estate agents to
                    prioritize the connections they’ve created.
                  </p>
                  <p className="div-section text-white text-lg-left mb-0">
                    As our technology came together piece by piece, we cemented
                    bonds between people, data, systems, and communication,
                    rewarding everyone who touches real estate with insights
                    beyond compare.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="tech-body">
            <h2 className="divide-section">The RealtorCenter</h2>
            <p className="div-section mb-0">
            The Alvin Grey RealtorCenter Web Application gives you the tools you need to onboard new downlines, manage your clients, track and claim your commissions, market your properties and showcase your achievements and testimonials. It is the virtual office you need to supercharge your Realtor business.

            </p>
          </div>
        <Footer />
        </div>
      </body>
    </div>
  );
}
