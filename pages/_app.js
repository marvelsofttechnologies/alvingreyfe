import '../styles/globals.css';
import Head from "next/head";

function MyApp({ Component, pageProps }) {
  return (
    <>
    <Head>
        <title>Alvin Grey</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://cdn.jsdelivr.net/gh/hung1001/font-awesome-pro@0ac23ca/css/all.css"
          rel="stylesheet"
        />
        <script type='text/javascript' src='/main.js' />the 
      </Head>
  <Component {...pageProps} />
  </>
  )
}

export default MyApp;
