import React from 'react'
import TopNav from '../components/Navigation/TopNav'

export default function learn() {
    return (
        <div>
            <TopNav />
        <div>
          <div className="learn-wrapper">
              <div className="learn-content">
            <h1 className="headline">Enjoy Your Very Own Library of Real Estate Learning Resources for Free</h1>
            <p className='text-center'>Get access to over 100+ hours of curated articles, video, and audio content on a wide variety of topics from digital sales and marketing to lead generation and relationship management. Our partner platform, Learn by Oxygen, provides all the learning resources you need and you can access everything right from the RealtorCenter application.</p>
            <h4 className="text-center">Sign up for our Newsletter to get notified when there is fresh content available.</h4>
            <div className="input-box text-center">
                  {/* <label className="input-label">Email</label> */}
                  <input
                    type="text"
                    className="form-input text-center w-50"
                    placeholder="Enter your Email"
                    required
                  />
                </div>
              </div>
          </div>
        </div>
        </div>
    )
}
